package br.com.mastertech.contato.services;

import br.com.mastertech.contato.models.Contato;
import br.com.mastertech.contato.repositories.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {
    @Autowired
    private ContatoRepository repository;

    public List<Contato> getAllByIdUsuario(Integer idUsuario) {
        return this.repository.findAllByIdUsuario(idUsuario);
    }

    public Contato criar(Contato contato) {
        return this.repository.save(contato);
    }
}