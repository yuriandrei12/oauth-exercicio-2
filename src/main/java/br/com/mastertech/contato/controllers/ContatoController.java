package br.com.mastertech.contato.controllers;

import br.com.mastertech.contato.models.Contato;
import br.com.mastertech.contato.security.Usuario;
import br.com.mastertech.contato.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
public class ContatoController {

    @Autowired
    private ContatoService service;

    @PostMapping("/contato")
    public Contato criarContato(@RequestBody Contato contato, @AuthenticationPrincipal Usuario usuario) {
        contato.setIdUsuario(usuario.getId());
        return this.service.criar(contato);
    }

    @GetMapping("/contatos")
    public List<Contato> getAllByIdUsuario(@AuthenticationPrincipal Usuario usuario) {
        return this.service.getAllByIdUsuario(usuario.getId());
    }

}